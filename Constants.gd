extends Node


# Space Vroom, an unoriginal space racer.
# Copyright (C) 2021  Yuval Langer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


const on_floor_maximum_distance := 0.1


# width, height, depth
const road_box_dimensions: Vector3 = Vector3(0.35, 0.5, 0.95)


const fall_acceleration: float = 50.0
const on_the_floor_fall_acceleration: float = 1.0
const jump_impulse: float = 8.0
const maximum_forward_speed: float = 20.0
const forward_speed_increment: float = 0.5
const side_speed: float = 1.0
