tool
extends Spatial



onready var small_spheres: = $CSGSphere/SmallSpheres


export var radius_of_little_spheres: float = 0.1 setget setget_radius
export var number_of_little_spheres: int = 3 setget setget_number
export var distance_from_axis_of_little_spheres: float = 0.34 setget setget_distance
export var height_of_little_spheres: float = 0.924 setget setget_height



func _ready() -> void:
	set_spheres()


func setget_height(height):
	height_of_little_spheres = height
	set_spheres()

func setget_distance(distance):
	distance_from_axis_of_little_spheres = distance
	set_spheres()

func setget_number(number):
	number_of_little_spheres = number
	set_spheres()
func setget_radius(radius):
	radius_of_little_spheres = radius
	set_spheres()


func set_spheres() -> void:
	remove_existing_spheres()
	add_little_spheres()


func remove_existing_spheres() -> void:
	print_debug(small_spheres)
	var small_sphere_children: = small_spheres.get_children()
	for small_sphere in small_sphere_children:
		small_spheres.remove_child(small_sphere)


func add_little_spheres() -> void:
	for i in range(number_of_little_spheres):
		var x: = distance_from_axis_of_little_spheres * sin(TAU * i/number_of_little_spheres)
		var z: = distance_from_axis_of_little_spheres * cos(TAU * i/number_of_little_spheres)
		var sphere: = CSGSphere.new()
		sphere.translation.x = x
		sphere.translation.z = z
		sphere.translation.y = height_of_little_spheres
		sphere.radius = radius_of_little_spheres
		small_spheres.add_child(sphere)
		print_debug("%s %s" % [x, z])
